Keyboard layout/model visualization.

Use XKB geometry data to render a keyboard.
The geometry data is parsed by x11, this removes the need for a bespoke XKB language implementation.

Requires xcb platform plugin to run.

To run it simply build and run. Should show a layout. A single argument sets a different model for rendering.
Layout may only be set via code currently.
