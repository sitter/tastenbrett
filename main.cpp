﻿/*
    Copyright 2019 Harald Sitter <sitter@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QColor>
#include <QDebug>
#include <QGuiApplication>
#include <QKeyEvent>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QX11Info>

#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <X11/extensions/XKBgeom.h>
#include <X11/extensions/XKBrules.h>
#include <X11/extensions/XKBstr.h>

#include <xkbcommon/xkbcommon.h>

#warning debugging hack used in main to impl a list function
#include "xkb_rules.h"

// xlib defines Bool which messes with QMetaType::Bool :'D
#undef Bool
// also defines KeyPress/Relese which messes with QEvent::Key*
#undef KeyPress
#undef KeyRelease

// Simple QGuiApp overlay to gobble up all key events for input-detection.
class Application final : public QGuiApplication
{
    Q_OBJECT
public:
    using QGuiApplication::QGuiApplication;

    bool notify(QObject *receiver, QEvent *event) override
    {
        if (event->type() == QEvent::KeyPress || event->type() == QEvent::KeyRelease) {
            emit keyEvent(dynamic_cast<QKeyEvent *>(event));
        }
        return QGuiApplication::notify(receiver,event);
    }

    static Application *instance()
    {
        return qobject_cast<Application *>(qGuiApp);
    }

signals:
    void keyEvent(QKeyEvent *event);
};

// hash of name to color from rgb.txt
static QHash<QString, QColor> x11Colors()
{
    QHash<QString, QColor> colors;

#warning todo get more exhaustive list across platform
    static QStringList txtPaths = {
        "/usr/share/X11/rgb.txt"
    };

    for (const auto &path : txtPaths) {
        QFile file(path);
        if (!file.open(QIODevice::ReadOnly)) {
            continue;
        }

        QByteArray line;
        QStringList list;
        while (!file.atEnd()) {
            line = file.readLine();

            int red = 0;
            int green = 0;
            int blue = 0;
            int pos = 0;
            if (sscanf(line, "%d %d %d%n", &red, &green, &blue, &pos) == 3) {
                const QString name = line.mid(pos).trimmed();
                const QColor color(red, green, blue);
                if (color.isValid()) {
                    colors[name] = color;
                }
            }
        }

        break; // found a file
    }

    return colors;
}

static QColor fromXKBColor(const XkbColorRec &color)
{
    // Unfortunately the color spec is explicitly not defined, so
    // it's up to the xserver implementation. Also unfortunate
    // is that Qt no longer seems to have capabilities of
    // natively handling x11 colors specs, so this is literally
    // a shot in the dark.
    // XParseColor is how one would get that data, but then we need
    // a color map, and for that we need a visual and a window....
    // all in all a pile of crap.
    //
    // https://en.wikipedia.org/wiki/X11_color_names

    static auto colors = x11Colors();

    const QString spec = QString::fromUtf8(color.spec);

    // Resolve through rgb.txt
    QColor ret = colors.value(spec);
    if (ret.isValid()) {
        return ret;
    }

    // Fall back to maybe resolve it internally somehow.
    // This will pretty much never work as most colors aren't
    // straight forward black, green, etc.
    ret.setNamedColor(spec);
    if (ret.isValid()) {
        return ret;
    }

    // NB: we implicitly rely on some values not mapping!
    //   e.g. doodad indicators may have green30 as off_color
    //   green30 is not a valid color, so this is implicitly
    //   meant to go nowhere as otherwise on would be green and off too
    //   ... quite the mess.

    qDebug() << "failed to map x11 color!" << color.spec;

    return QColor(Qt::gray);
}

static QString xkbKeysymToName(xkb_keysym_t keysym)
{
    QVarLengthArray<char, 32> chars(32);
    Q_ASSERT(chars.size() >= 0); // ensure cast to size_t

    const int size = xkb_keysym_get_name(keysym, chars.data(), static_cast<size_t>(chars.size()));
    if (Q_UNLIKELY(size > chars.size())) {
        chars.resize(size);
        xkb_keysym_get_name(keysym, chars.data(), static_cast<size_t>(chars.size()));
    }

    return QString::fromUtf8(chars.constData(), size);
}

static QString xkbKeysymToUtf8(xkb_keysym_t keysym)
{
    QVarLengthArray<char, 32> chars(32);
    Q_ASSERT(chars.size() >= 0); // ensure cast to size_t

    const int size = xkb_keysym_to_utf8(keysym, chars.data(), static_cast<size_t>(chars.size()));
    if (Q_UNLIKELY(size > chars.size())) {
        chars.resize(size);
        xkb_keysym_to_utf8(keysym, chars.data(), static_cast<size_t>(chars.size()));
    }

    return QString::fromUtf8(chars.constData(), size);
}

static QString keySymToString(KeySym keysym) {
    // Strangely enough xkbcommons's UTF map is incomplete with regards to
    // dead keys. Extend it a bit.
    static QHash<unsigned long, char> deadMap {
        { XK_dead_grave, 0x0060 },
        { XK_dead_acute, 0x00b4 },
        { XK_dead_circumflex, 0x02c6 },
        { XK_dead_tilde, 0x02dc },
        { XK_dead_macron, 0x00af },
        { XK_dead_breve, 0x02D8 },
        { XK_dead_abovedot, 0x02D9 },
        { XK_dead_diaeresis, 0x00A8 },
        { XK_dead_abovering, 0x02DA },
        { XK_dead_doubleacute, 0x02DD },
        { XK_dead_caron, 0x02C7 },
        { XK_dead_cedilla, 0x00B8 },
        { XK_dead_ogonek, 0x02DB },
        { XK_dead_iota, 0x0269 },
        { XK_dead_voiced_sound, 0x309B },
        { XK_dead_semivoiced_sound, 0x309A },
        { XK_dead_belowdot, 0x0323 },
        { XK_dead_hook, 0x0309 },
        { XK_dead_horn, 0x031b },
        { XK_dead_stroke, 0x0335 },
        { XK_dead_abovecomma, 0x0312 },
        { XK_dead_abovereversedcomma, 0x0314 },
        { XK_dead_doublegrave, 0x030f },
        { XK_dead_belowring, 0x0325 },
        { XK_dead_belowmacron, 0x0331 },
        { XK_dead_belowcircumflex, 0x032D },
        { XK_dead_belowtilde, 0x0330 },
        { XK_dead_belowbreve, 0x032e },
        { XK_dead_belowdiaeresis, 0x0324 },
        { XK_dead_invertedbreve, 0x0311 },
        { XK_dead_belowcomma, 0x0326 },
        { XK_dead_currency, 0x00A4 },
        { XK_dead_a, 0x0061 },
        { XK_dead_A, 0x0041 },
        { XK_dead_e, 0x0065 },
        { XK_dead_E, 0x0045 },
        { XK_dead_i, 0x0069 },
        { XK_dead_I, 0x0049 },
        { XK_dead_o, 0x006f },
        { XK_dead_O, 0x004f },
        { XK_dead_u, 0x0075 },
        { XK_dead_U, 0x0055 },
        { XK_dead_small_schwa, 0x0259 },
        { XK_dead_capital_schwa, 0x018F },
    };

    // XKB has fairly OK unicode maps, unfortunately it is
    // overzaelous and will for example return "U+001B" for
    // Esc which is a non-printable control character and
    // also not present in most fonts. As such it is
    // worthless to use and we'll discard unicode strings that
    // contain non-printable characters (ignore null).
    // This will lead to one of the stringy name fallbacks to handle
    // these cases and produce for example 'Escape'

    qDebug() << "keysym" << keysym << XKeysymToString(keysym);

    QString str;

    // Smartly xlib uses ulong and xkbcommon uses uint32 for syms,
    // so we'd best make sure that we can even cast the symbol before
    // tryint to do xkb mappings. Otherwise skip to fallbacks right away.
    const xkb_keysym_t xkbKeysym = static_cast<xkb_keysym_t>(keysym) ;
    if (static_cast<KeySym>(xkbKeysym) == keysym) {
        str = xkbKeysymToUtf8(xkbKeysym);
        qDebug() << "xkb UTF8 translated:" << str.toUtf8();

        for (const auto &c : str) {
            if (!c.isPrint() && !c.isNull()) {
                qDebug() << "XKB unicode not printable... resettting!";
                str = "";
                break;
            }
        }

        if (str.isEmpty()) {
            str = xkbKeysymToName(xkbKeysym);
            qDebug() << "xkb translated:" << qUtf8Printable(str);
        }
    }

    if (str.isEmpty()) {
        str = XKeysymToString(keysym);
        qDebug() << "x11 translated:" << qUtf8Printable(str);
    }

    if (deadMap.contains(keysym)) {
        str = QChar(deadMap[keysym]);
        qDebug() << "deadmap translated:" << qUtf8Printable(str);
    }

#warning maybe split by underscore as x11 keys can contain them
    return str;
}

class XkbObject : public QObject
{
protected:
    XkbObject(XkbDescPtr xkb_, QObject *parent = nullptr)
        : QObject(parent)
        , xkb(xkb_)
    {
        Q_ASSERT(xkb);
    }

    XkbDescPtr xkb = nullptr;
};

class Outline : public XkbObject
{
    Q_OBJECT
#define P(type, name) \
private: \
    Q_PROPERTY(type name READ auto_prop_##name CONSTANT) \
public: \
    type auto_prop_##name () const { return outline-> name ; }

    P(unsigned short, corner_radius)

    Q_PROPERTY(QVariantList points MEMBER points CONSTANT)

public:
    Outline(XkbOutlinePtr outline_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : XkbObject(xkb_, parent)
        , outline(outline_)
    {
        for (int i = 0; i < outline->num_points; ++i) {
            const auto p = outline->points + i;
            points.push_back(QPoint(p->x, p->y));
        }
    }

    XkbOutlinePtr outline = nullptr;
    QVariantList points;
};

class Shape : public XkbObject
{
    Q_OBJECT

    Q_PROPERTY(QList<QObject *> outlines MEMBER outlines CONSTANT)
    Q_PROPERTY(QRect bounds MEMBER bounds CONSTANT)
public:
    Shape(XkbShapePtr shape_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : XkbObject(xkb_, parent)
        , shape(shape_)
        , bounds(QRect(shape->bounds.x1, shape->bounds.y1,
                       shape->bounds.x2, shape->bounds.y2))
    {
        // FIXME: this is garbage for doodads we need to draw both outlines I think
        if (shape->primary) {
            outlines.push_back(new Outline(shape->primary, xkb, this));
            return;
        }

        for (int i = 0; i < shape->num_outlines; ++i) {
            outlines.push_back(new Outline(shape->outlines + i, xkb, this));
        }
    }

    XkbShapePtr shape = nullptr;
    QList<QObject *> outlines;
    QRect bounds;
};

class Item : public QObject
{
    Q_OBJECT
public:
};

// This is a fairly opinionated model of a key cap. We assume there won't
// be any more than 4 levels.
// This isn't necessarily the case since (e.g.) the NEO layout has 6 levels.
// Since that is super niche and poses some complication as for how to
// exactly render the levels we'll simply ignore this use case for now.
// In the end the keyboard model display only needs to act as simple indicator
// anyway.
class KeyCap : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString topLeft MEMBER topLeft CONSTANT)
    Q_PROPERTY(QString topRight MEMBER topRight CONSTANT)
    Q_PROPERTY(QString bottomLeft MEMBER bottomLeft CONSTANT)
    Q_PROPERTY(QString bottomRight MEMBER bottomRight CONSTANT)
public:
    enum Level
    {
        TopLeft = 1,
        TopRight = 3,
        BottomLeft = 0,
        BottomRight = 2
    };
    Q_ENUM(Level)
    constexpr static int levelCount = 4;

    KeyCap(const QString symbols[KeyCap::levelCount], QObject *parent)
        : QObject(parent)
        , topLeft(symbols[Level::TopLeft])
        , topRight(symbols[Level::TopRight])
        , bottomLeft(symbols[Level::BottomLeft])
        , bottomRight(symbols[Level::BottomRight])
    {
    }

    QString topLeft;
    QString topRight;
    QString bottomLeft;
    QString bottomRight;
};

static QColor invertColor(QColor color)
{
    return QColor(255 - color.red(),
                  255 - color.green(),
                  255 - color.blue());
}

class Key : public XkbObject
{
    Q_OBJECT
#define P(type, name) \
private: \
    Q_PROPERTY(type name READ auto_prop_##name CONSTANT) \
public: \
    type auto_prop_##name () const { return key-> name ; }

    P(short, gap)

    Q_PROPERTY(Shape *shape MEMBER shape CONSTANT)
    Q_PROPERTY(KeyCap *cap MEMBER cap CONSTANT)
    Q_PROPERTY(QColor color MEMBER color CONSTANT)
    // Purely here to allow counter-indication of a key press.
    // i.e. default color is color and when pressed we use its inverse
    // to ensure contrast.
    Q_PROPERTY(QColor inverseColor MEMBER inverseColor CONSTANT)
    Q_PROPERTY(bool pressed MEMBER pressed NOTIFY pressedChanged)

    constexpr static uint INVALID_KEYCODE = static_cast<uint>(-1);

public:
    Key(XkbKeyPtr key_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : XkbObject(xkb_, parent)
        , key(key_)
        , shape(new Shape(xkb->geom->shapes + key->shape_ndx, xkb, this))
        , nativeScanCode(nativeScanCodeFromName(key->name.name))
        , cap(resolveCap())
        , color(fromXKBColor(xkb->geom->colors[key->color_ndx])) // qGuiApp->palette().color(QPalette::Active, QPalette::Highlight))
        , inverseColor(invertColor(color))
        , pressed(false)
    {
        connect(Application::instance(), &Application::keyEvent,
                this, [this](QKeyEvent *event)
        {
            Q_ASSERT(event);
            if (event->nativeScanCode() == nativeScanCode) {
                pressed = event->type() == QKeyEvent::KeyPress;
                emit pressedChanged();
            }
        });
    }

    uint nativeScanCodeFromName(const char *keyNameCString)
    {
        const QByteArray needle(keyNameCString, XkbKeyNameLength);

        for (uint keycode = xkb->min_key_code; keycode <= xkb->max_key_code; ++keycode) {
            XkbKeyNameRec key = xkb->names->keys[keycode];
            const QByteArray name(key.name, XkbKeyNameLength);
            if (name == needle) {
                return keycode;
            }
        }

        for (int i = 0; i < xkb->names->num_key_aliases; ++i) {
            XkbKeyAliasRec alias = xkb->names->key_aliases[i];
            const QByteArray name(alias.alias, XkbKeyNameLength);
            if (name == needle) {
                return nativeScanCodeFromName(alias.real);
            }
        }

        return INVALID_KEYCODE;
    }

    KeyCap *resolveCap()
    {
        // Documentation TLDR
        // - Levels are accessed by a modifier switching the keyboard to different symbols
        //   such as hitting Shift and getting access to Shift+1=!
        // - Groups are an additional system which considers the entire keyboard switched
        //   to a different symbol set. Such as the entire keyboard being Latin or Cyrillic.
        //   Within each group there may be N Levels. The keyboard therefor has N Groups with
        //   each having M levels.
        // For the purposes of key cap resolution we'll only look at the first group and the
        // first 4 levels within that group (top-left, top-right, bottom-left, bottom-right).

        const quint32 keycode = nativeScanCode;
        QString symbols[KeyCap::levelCount];

        if (keycode == INVALID_KEYCODE) {
            return new KeyCap(symbols, this);
        }

        const int group = 0;
        // We iterate over the enum directly because it also represents
        // preference. TopLeft is the preferred location for unique mapping
        // such as 'F1' that can appear in all levels but we only want it shown
        // once in the TopLeft position.
        const auto levelEnum = QMetaEnum::fromType<KeyCap::Level>();
        QVector<QString> seen;
        for (int i = 0; i < levelEnum.keyCount(); ++i) {
            int level = levelEnum.value(i);
            if (group >= XkbKeyNumGroups(xkb, keycode)) {
                continue; // group doesn't exist, shouldn't happen because we use group0
            }
            if (level >= XkbKeyGroupWidth(xkb, keycode, group)) {
                continue; // level within group doesn't exist, can totally happen!
            }

            KeySym keysym = XkbKeySymEntry(xkb, keycode, level, group);
            const auto str = keySymToString(keysym);
            if (seen.contains(str)) {
                // Don't duplicate. e.g. 'F1' can appear in all levels
                continue;
            }
            seen << str;
            symbols[level] = str;
        }

        return new KeyCap(symbols, this);
    }

    XkbKeyPtr key = nullptr;
    Shape *shape = nullptr;
    quint32 nativeScanCode = INVALID_KEYCODE;
    KeyCap *cap = nullptr;
    QColor color;
    QColor inverseColor;
    bool pressed = false;

signals:
    void pressedChanged();
};

class Doodad : public XkbObject
{
    Q_OBJECT

#define P(type, name) \
private: \
    Q_PROPERTY(type name READ auto_prop_##name CONSTANT) \
public: \
    type auto_prop_##name () const { return doodad->any. name ; }

    P(unsigned char, priority)

public:
    Doodad(XkbDoodadPtr doodad_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : XkbObject(xkb_, parent)
        , doodad(doodad_)
    {
    }

    XkbDoodadPtr doodad = nullptr;
};

#warning logo not mapped

class ShapeDoodad : public Doodad
{
    Q_OBJECT

#define P(type, name) \
private: \
    Q_PROPERTY(type name READ auto_prop_##name CONSTANT) \
public: \
    type auto_prop_##name () const { return doodad->shape. name ; }

    P(short, top)
    P(short, left)
    P(short, angle)

    Q_PROPERTY(Shape *shape MEMBER shape CONSTANT)
    Q_PROPERTY(QColor color MEMBER color CONSTANT)

public:
    ShapeDoodad(XkbDoodadPtr doodad_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : Doodad(doodad_, xkb_, parent)
        , shape(new Shape(xkb->geom->shapes + doodad_->shape.shape_ndx, xkb, this))
        , color(fromXKBColor(xkb->geom->colors[doodad->shape.color_ndx]))
    {
    }

    Shape *shape = nullptr;
    QColor color;
};

class TextDoodad : public Doodad
{
    Q_OBJECT

#define P(type, name) \
private: \
    Q_PROPERTY(type name READ auto_prop_##name CONSTANT) \
public: \
    type auto_prop_##name () const { return doodad->text. name ; }

    P(short, top)
    P(short, left)
    P(short, angle)
    P(short, width)
    P(short, height)
    P(QString, text)
    P(QString, font)

    Q_PROPERTY(QColor color MEMBER color CONSTANT)

public:
    TextDoodad(XkbDoodadPtr doodad_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : Doodad(doodad_, xkb_, parent)
        , color(fromXKBColor(xkb->geom->colors[doodad->text.color_ndx]))
    {
    }

    Shape *shape = nullptr;
    QColor color;
};

// FIXME: this is kind of a ShapeDooda BUT this needs internal rigging bc we cannot access the same
// data via doodad->shape it seems, so based on the class we need to access through doodad->shape or doodad->indicator
// this is kinda hard to polymorph
class IndicatorDoodad : public Doodad
{
    Q_OBJECT

#define P(type, name) \
private: \
    Q_PROPERTY(type name READ auto_prop_##name CONSTANT) \
public: \
    type auto_prop_##name () const { return doodad->indicator. name ; }

    P(short, top)
    P(short, left)
    P(short, angle)

    Q_PROPERTY(Shape *shape MEMBER shape CONSTANT)
    Q_PROPERTY(bool on MEMBER on NOTIFY onChanged)

    Q_PROPERTY(QColor onColor MEMBER onColor CONSTANT)
    Q_PROPERTY(QColor offColor MEMBER offColor CONSTANT)
public:
    IndicatorDoodad(XkbDoodadPtr doodad_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : Doodad(doodad_, xkb_, parent)
        , shape(new Shape(xkb->geom->shapes + doodad_->indicator.shape_ndx, xkb, this))
        , onColor(fromXKBColor(xkb->geom->colors[doodad->indicator.on_color_ndx]))
        , offColor(fromXKBColor(xkb->geom->colors[doodad->indicator.off_color_ndx]))
    {
        connect(Application::instance(), &Application::keyEvent,
                this, &IndicatorDoodad::refreshState);
        refreshState();
    }

    Shape *shape = nullptr;
    QColor onColor;
    QColor offColor;
    bool on = false;

signals:
    void onChanged();

private slots:
    void refreshState()
    {
        int onInt = False;
        if (!XkbGetNamedIndicator(QX11Info::display(),
                                  doodad->indicator.name,
                                  nullptr,
                                  &onInt,
                                  nullptr,
                                  nullptr)) {
            on = false;
        } else {
            on = onInt == True ? true : false;
        }
        emit onChanged();
    }
};

class Row : public XkbObject
{
    Q_OBJECT

#define P(type, name) \
private: \
    Q_PROPERTY(type name READ auto_prop_##name CONSTANT) \
public: \
    type auto_prop_##name () const { return row-> name ; }

P(short, top)
P(short, left)
Q_PROPERTY(Qt::Orientation orientation MEMBER orientation CONSTANT)

Q_PROPERTY(QList<QObject *> keys MEMBER keys CONSTANT)
Q_PROPERTY(QRect bounds MEMBER bounds CONSTANT)
public:
    Row(XkbRowPtr row_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : XkbObject(xkb_, parent)
        , row(row_)
        , orientation(row->vertical ? Qt::Vertical : Qt::Horizontal)        
        , bounds(QRect(row->bounds.x1, row->bounds.y1,
                       row->bounds.x2, row->bounds.y2))
    {
        for (int i = 0; i < row->num_keys; ++i) {
            keys.push_back(new Key(row->keys + i, xkb, this));
        }
    }

    XkbRowPtr row = nullptr;
    Qt::Orientation orientation = Qt::Horizontal;
    QList<QObject *> keys;
    QRect bounds;
};

class Section : public XkbObject
{
    Q_OBJECT

#define P(type, name) \
private: \
    Q_PROPERTY(type name READ auto_prop_##name CONSTANT) \
public: \
    type auto_prop_##name () const { return section-> name ; }

    P(unsigned char, priority)
    P(short, top)
    P(short, left)
    P(unsigned short, width)
    P(unsigned short, height)
    P(short, angle)

    Q_PROPERTY(QList<QObject *> rows MEMBER rows CONSTANT)
    Q_PROPERTY(QList<QObject *> doodads MEMBER doodads CONSTANT)
public:
    Section(XkbSectionPtr section_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : XkbObject(xkb_, parent)
        , section(section_)
    {
        for (int i = 0; i < section->num_doodads; ++i) {
            auto doodad = section->doodads + i;

#warning fixme codedupe from geom
            Doodad *o = nullptr;
            switch (doodad->any.type) {
            case XkbOutlineDoodad:
            case XkbSolidDoodad:
                o = new ShapeDoodad(doodad, xkb, this);
                qDebug() << "shape";
                break;

            case XkbTextDoodad:
                o = new TextDoodad(doodad, xkb, this);
                qDebug() << "text";
                break;

            case XkbIndicatorDoodad:
                o = new IndicatorDoodad(doodad, xkb, this);
                qDebug() << "indicator";
                break;

            case XkbLogoDoodad:
                qDebug() << "logo";
                o = new ShapeDoodad(doodad, xkb, this);
                break;
            }

            doodads.push_back(o);
        }

        for (int i = 0; i < section->num_rows; ++i) {
            rows.push_back(new Row(section->rows + i, xkb, this));
        }
    }

    XkbSectionPtr section = nullptr;
    QList<QObject *> rows;
    QList<QObject *> doodads;
};

class Geometry : public XkbObject
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject *> doodads MEMBER doodads CONSTANT)
    Q_PROPERTY(QList<QObject *> sections MEMBER sections CONSTANT)
public:
    Geometry(XkbGeometryPtr geom_, XkbDescPtr xkb_, QObject *parent = nullptr)
        : XkbObject(xkb_, parent)
        , geom(geom_)
        , baseColor(fromXKBColor(*geom->base_color))
        , labelColor(fromXKBColor(*geom->label_color))
    {
        for (int i = 0; i < geom->num_doodads; ++i) {
            auto doodad = geom->doodads + i;

            Doodad *o = nullptr;
            switch (doodad->any.type) {
            case XkbOutlineDoodad:
            case XkbSolidDoodad:
                o = new ShapeDoodad(doodad, xkb, this);
                qDebug() << "shape";
                break;

            case XkbTextDoodad:
                o = new TextDoodad(doodad, xkb, this);
                qDebug() << "text";
                break;

            case XkbIndicatorDoodad:
                o = new IndicatorDoodad(doodad, xkb, this);
                qDebug() << "indicator";
                break;

            case XkbLogoDoodad:
//                o = new ShapeDoodad(doodad);
                qDebug() << "logo";
                break;
            }

            if (!o) { // bc of logo dooda
                continue;
            }

            doodads.push_back(o);
        }

        for (int i = 0; i < geom->num_sections; ++i) {
            sections.push_back(new Section(geom->sections + i, xkb, this));
        }
    }

    XkbGeometryPtr geom = nullptr;

    QList<QObject *> doodads;
    QList<QObject *> sections;

    QColor baseColor;
    QColor labelColor;
};

int main(int argc, char *argv[])
{
    setenv("QT_QPA_PLATFORM", "xcb", 1);
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    Application app(argc, argv);
    Q_ASSERT(app.platformName() == QStringLiteral("xcb"));

#warning debuggy functionality to list all models... codecopy from kcm
    if (app.arguments().value(1, QString()) == QString("-l")) {
        auto r = Rules::readRules(Rules::READ_EXTRAS);
        for (auto m : r->modelInfos) {
            printf("%s\n", qUtf8Printable(m->name));
        }
        return 0;
    }

    XkbRF_VarDefsRec varDefs;
    memset(&varDefs, 0, sizeof(XkbRF_VarDefsRec));

    QString model;
//    model = "kinesis"; // fancy model
//    model = "tm2020"; // fancy model
    model = "pc101";
    model = app.arguments().value(1, model);
    QString layout = "at";

    // Hold these so so we can pass data into xkb getter.
    QByteArray modelArray = model.toUtf8();
    QByteArray layoutArray = layout.toUtf8();

    varDefs.model = modelArray.data();
    varDefs.layout = layoutArray.data();
    varDefs.variant = nullptr;
    varDefs.options = nullptr;

    // TODO: can this be made more dynamic? can anything but evdev be used?
    XkbRF_RulesPtr rules = XkbRF_Load("/usr/share/X11/xkb/rules/evdev",
                                      qgetenv("LOCALE").data(),
                                      True,
                                      True);
    Q_ASSERT(rules);
    QSharedPointer<XkbRF_RulesRec> rulesCleanup(rules, [](XkbRF_RulesPtr obj) {
        XkbRF_Free(obj, True);
    });

    XkbComponentNamesRec componentNames;
    memset(&componentNames, 0, sizeof(XkbComponentNamesRec));

    XkbRF_GetComponents(rules, &varDefs, &componentNames);

    XkbDescPtr xkb = XkbGetKeyboardByName(QX11Info::display(),
                                          XkbUseCoreKbd,
                                          &componentNames,
                                          0,
                                          XkbGBN_GeometryMask |
                                          XkbGBN_KeyNamesMask |
                                          XkbGBN_OtherNamesMask |
                                          XkbGBN_ClientSymbolsMask |
                                          XkbGBN_IndicatorMapMask,
                                          false);
    Q_ASSERT(xkb);
    QSharedPointer<XkbDescRec> xkbCleanup(xkb, [](XkbDescPtr obj) {
        XkbFreeKeyboard(obj, 0, True);
    });

    Geometry geometry(xkb->geom, xkb);

    // The way this is currently written we need the engine after
    // we have a geometry, lest geometry is dtor'd before the engine
    // causing exhaustive error spam on shutdown.
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.rootContext()->setContextProperty("geometry", &geometry);
    engine.load(url);

    return app.exec();
}

#include "main.moc"
