/*
    Copyright 2019 Harald Sitter <sitter@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.5

Canvas {
    property QtObject shape
    property variant strokeSyle: "yellow"
    property variant fillStyle: "steelblue"
    // Only draw one outline. Outlines are tricky because xkb has no
    // concept of displaying text on top of the shapes. IOW: it
    // only thinks about blank keys. This means we have no layout
    // constraints for our text and as a result we'd have to figure out
    // where to put text so it doesn't conflict with any of the drawn
    // paths and is inside most/all of them. It's not worth the work...
    property int maxOutline: 1

    id: canvas
    width: shape.bounds.width
    height: shape.bounds.height
    onStrokeSyleChanged: requestPaint()
    onFillStyleChanged: requestPaint()

    onPaint: {
        var ctx = getContext("2d")
        ctx.lineWidth = 4
        ctx.strokeStyle = strokeSyle
        ctx.fillStyle = fillStyle

        ctx.beginPath()

        for (var i in shape.outlines) {
            if (maxOutline && i >= maxOutline) {
                break;
            }

            var outline = shape.outlines[i]

            if (outline.points.length === 1) { // rect from 0,0 to point
                var point = outline.points[0]

                ctx.roundedRect(0, 0,
                                point.x, point.y,
                                outline.corner_radius, outline.corner_radius)
            } else if (outline.points.length === 2) { // rect from p1 to p2
                var topLeftPoint = outline.points[0]
                var bottomRightPoint = outline.points[1]

                ctx.roundedRect(topLeftPoint.x, topLeftPoint.y,
                                bottomRightPoint.x, bottomRightPoint.y,
                                outline.corner_radius, outline.corner_radius)
            } else { // polygon point to point draw (used for doodads and non-rect keys such as Enter)
                for (var j in outline.points) {
                    var anyPoint = outline.points[j]
                    if (j < 1) { // move to first point
                        ctx.moveTo(anyPoint.x, anyPoint.y)
                    } else { // from there we draw point to point
                        ctx.lineTo(anyPoint.x, anyPoint.y)
                    }

                    // TODO: should probably draw short of target and then arcTo over the target so we get a round corner?
                    // Currently shapes are not rounded.
                }
            }
        }

        ctx.closePath()
        if (fillStyle) {
            ctx.fill()
        }
        ctx.stroke()
    }
}
