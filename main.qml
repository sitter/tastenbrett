/*
    Copyright 2019 Harald Sitter <sitter@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5

Window {
    id: window
    visible: false
    width: 1024
    height: 500
    title: qsTr("Hello World")

    Item {
        id: kbd
        width: window.width
        height: window.height

        // XKB geometries have a consistent coordinate system within a geometry
        // all children follow that system instead of QML. To easily convert
        // everything wholesale we'll employ a scale factor.
        property real childWidth: childrenRect.x + childrenRect.width
        property real childHeight: childrenRect.y + childrenRect.height
        // We preserver the ratio and as a result will need to scall either
        // with height or width, whichever has less space.
        scale: Math.min(width / childWidth, height / childHeight)
        transformOrigin: Item.TopLeft
    }

    Component {
        id: keyComponent

        // Interactable Key
        Key {
            id: root

            MouseArea {
                id: hoverArea
                anchors.fill: parent
                hoverEnabled: true
            }

            ToolTip {
                visible: hoverArea.containsMouse

                contentItem: Item {
                    // FIXME: width should really be calculated based on bounds relative to width
                    //   i.e. currently we assume keys are square, when they are often not (e.g. shift)
                    implicitWidth: 128
                    implicitHeight: 128
                    Key {
                        id: keyItem
                        key: root.key
                        anchors.fill: parent
                    }
                }

                background: Rectangle {
                    color: root.key.inverseColor
                    border.color: root.key.inverseColor
                    border.width: 4
                }
            }
        }
    }

    Component {
        id: doodadShapeComponent
        ShapeDoodad {}
    }

    Component {
        id: doodadIndicatorComponent
        IndicatorDoodad {}
    }

    Component {
        id: doodadTextComponent
        TextDoodad {}
    }

    Component {
        id: rowComponent

        Item {
            property QtObject row: null
            x: row.left
            y: row.top
            width: childrenRect.width+4
            height: childrenRect.height+4

            Component.onCompleted: {
                for (var i in row.keys) {
                    var key = row.keys[i]
                    keyComponent.createObject(this, { key: key });
                }
            }
        }
    }

    Component {
        id: sectionComponent

        Item {
            property QtObject section
            x: section.left
            y: section.top
            z: section.priority
            // TODO: figure out what to do with these... most geoms have sections that extend way too far
            //   to the right blowing up the entire keyboard Item for literally no reason!
//            width: section.width
//            height: section.height
            width: childrenRect.width
            height: childrenRect.height

            // Fix rotation to mod90, we cannot spin around as that'd put the text upside down ;)
            // Unclear if spinning around like that is in fact desired for anyting, if so I guess
            // we need to counter rotate text or something.
            rotation: section.angle != 0 ? section.angle % 90 : section.angle
            transformOrigin: Item.TopLeft

            Component.onCompleted: {
                for (var i in section.rows) {
                    console.debug(i)
                    var row = section.rows[i]
                    rowComponent.createObject(this, { row: row });
                }

                // TODO: unclear if sections can have doodads that aren't also in the geom
                //   we do render the geom sections already
            }
        }
    }

    Component.onCompleted: {
        for (var i in geometry.sections) {
            var section = geometry.sections[i]
            sectionComponent.createObject(kbd, { section: section });
        }

        for (var i in geometry.doodads) {
            var doodad = geometry.doodads[i]

            // FIXME hack
            if (doodad.onColor !== undefined) {
                doodadIndicatorComponent.createObject(kbd, { doodad: doodad });
            } else if (doodad.text !== undefined) {
                doodadTextComponent.createObject(kbd, { doodad: doodad });
            } else if (doodad.shape !== undefined) {
                doodadShapeComponent.createObject(kbd, { doodad: doodad });
            }
        }

        visible = true
    }
}
